import sublime, sublime_plugin

class ToggleRulerCommand(sublime_plugin.TextCommand):

    def run(self, edit):
        if not self.view.settings().has("former_rulers"):
            self.view.settings().set( "former_rulers", [70] )

        if self.view.settings().has("rulers") and len( self.view.settings().get("rulers") ) != 0:
            self.view.settings().set("former_rulers", self.view.settings().get("rulers"))
            self.view.settings().erase("rulers")
        else:
            self.view.settings().set("rulers", self.view.settings().get("former_rulers"))


class SetRulerCommand(sublime_plugin.WindowCommand):

    def run(self):
        default_ruler = 70
        if self.window.active_view().settings().has( "rulers" ) and len( self.window.active_view().settings().get("rulers") ) > 0:
            default_ruler = int(self.window.active_view().settings().get("rulers")[0])

        self.window.show_input_panel("Ruler Column", str(default_ruler), self.on_done, None, None )

    def on_done(self, text):
        try:
            col = int(text)
            if self.window.active_view():
                if col <= 0:
                    self.window.active_view().settings().erase("rulers");
                else:
                    self.window.active_view().settings().set("rulers", [col])
        except ValueError:
            pass


